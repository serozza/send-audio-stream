#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QTcpSocket>
#include <QMediaPlayer>
#include <QBuffer>
#include <QAudioOutput>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , mSock(new QTcpSocket(this))
    , mIncommingData(new QByteArray)
    , mBuffer(new QBuffer(mIncommingData.data()))
    , mAudioOutput(nullptr)
{
    ui->setupUi(this);

    connect(mSock, &QTcpSocket::readyRead, this, &MainWindow::slReadyRead);

    mBuffer->open(QBuffer::ReadWrite);

    QAudioFormat format;
       // Set up the format, eg.
       format.setSampleRate(8000);
       format.setChannelCount(2);
       format.setSampleSize(16);
       format.setCodec("audio/pcm");
       format.setByteOrder(QAudioFormat::LittleEndian);
       format.setSampleType(QAudioFormat::UnSignedInt);


       QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
       if (!info.isFormatSupported(format)) {
           qWarning() << "Raw audio format not supported by backend, cannot play audio.";
           return;
       }

       mAudioOutput = new QAudioOutput(format, this);

       mAudioOutput->start(mBuffer);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::connectToHost(QString host)
{
    mSock->connectToHost(host, 1024);

    return mSock->waitForConnected();
}

void MainWindow::handleCurrentAudioOutputState()
{
    if(mAudioOutput)
    {
        QAudio::State currentSatte = mAudioOutput->state();
        switch(currentSatte)
        {
        case QAudio::ActiveState: break;
        case QAudio::SuspendedState:break;
        case QAudio::StoppedState:
        case QAudio::IdleState:
        {
            mAudioOutput->start(mBuffer);
            break;
        }
        }
    }
}

void MainWindow::slReadyRead()
{
    qDebug() << "incomming data: " << mSock->size() ;

    mIncommingData->append(mSock->readAll());

    handleCurrentAudioOutputState();
}

void MainWindow::on_connectButton_clicked()
{
    connectToHost("127.0.0.1");
    ui->connectButton->hide();
}
