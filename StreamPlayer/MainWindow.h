#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QMediaPlayer;
class QTcpSocket;
class QBuffer;
class QAudioOutput;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    bool connectToHost(QString host);
    void handleCurrentAudioOutputState();

private slots:
    void slReadyRead();
    void on_connectButton_clicked();

private:
    Ui::MainWindow *ui;

    QTcpSocket* mSock;
    QScopedPointer<QByteArray> mIncommingData;
    QBuffer* mBuffer;
    QAudioOutput* mAudioOutput;
};

#endif // MAINWINDOW_H
