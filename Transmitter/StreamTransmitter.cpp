#include "StreamTransmitter.h"
#include <QTimer>
#include <QFile>
#include <QTcpSocket>
#include <QDebug>
#include <QCoreApplication>

StreamTransmitter::StreamTransmitter(int interval, int partsCount, QObject *parent)
    : QObject(parent)
    , mIncomingDataTimer(new QTimer(this))
    , mServer(new QTcpServer(this))
    , mTimerInterval(interval)
    , mPartsCount(partsCount)
{
    mIncomingDataTimer->setInterval(mTimerInterval);
    connect(mIncomingDataTimer, &QTimer::timeout, this, &StreamTransmitter::sendFewBytes);

    mServer->listen(QHostAddress::Any, 1024);

    connect(mServer, &QTcpServer::newConnection, this, &StreamTransmitter::slNewConnection );
}

void StreamTransmitter::run()
{

}

QByteArray StreamTransmitter::fileToByteArray(QString fileName)
{
    QFile file(fileName);

    file.open(QIODevice::ReadOnly);

    QByteArray retArr = file.readAll();

    file.close();

    return retArr;
}

void StreamTransmitter::sendFewBytes()
{
    static QByteArray track = fileToByteArray(QString(":/sound.wav"));

    qDebug() << "track size is: ";
    qDebug() << track.size();

    if(track.size() == 0)
    {
        qApp->quit();
    }


    static QList<QByteArray> parts = splitToParts(track, mPartsCount);
    static int currentPart = 0;

    if(parts.size() > 0 && currentPart < parts.size())
    {
        //        mSocket->write(parts[currentPart]);

        for (QTcpSocket* client : mClients)
        {
            client->write(parts[currentPart]);
        }

        qDebug() << "Outgoing data!";
        qDebug() << "Part: " << currentPart + 1 << "/" << parts.size();

        ++currentPart;
    }
    else
    {
       mIncomingDataTimer->stop();
    }
}

QList<QByteArray> StreamTransmitter::splitToParts(const QByteArray &arr, int parts)
{
    QList<QByteArray> retVal;

    int bytesInPart = arr.size() / parts;

    for (int i = 0; i < parts; ++i)
    {
        QByteArray tmpArr = arr.mid( bytesInPart * i, bytesInPart);

        retVal.push_back(tmpArr);
    }

    qDebug() << parts << " parts by " << retVal.size() << " bytes";

    return retVal;
}

void StreamTransmitter::slNewConnection()
{
    while (mServer->hasPendingConnections())
    {
        QTcpSocket *clientSocket = mServer->nextPendingConnection();

        mClients.push_back(clientSocket);

        connect(clientSocket, &QTcpSocket::readyRead, this, &StreamTransmitter::readyRead);
        connect(clientSocket, &QTcpSocket::disconnected, this, &StreamTransmitter::disconnected);

        mIncomingDataTimer->start();
    }
}

void StreamTransmitter::readyRead()
{
     QTcpSocket *socket = qobject_cast<QTcpSocket*>(sender());

     if(socket)
     {
        qDebug() << "Incoming data!";
     }
}

void StreamTransmitter::disconnected()
{

}
