#include <QCoreApplication>
#include <StreamTransmitter.h>
#include <QDebug>
#include <QString>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    StreamTransmitter* transmitter = (argc < 3)
            ? new StreamTransmitter(1000, 50, &a)
            : new StreamTransmitter(QString(argv[1]).toInt(), QString(argv[2]).toInt(), &a);

    transmitter->run();

    return a.exec();
}
