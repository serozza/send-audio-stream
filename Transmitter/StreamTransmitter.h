#ifndef STREAMTRANSMITTER_H
#define STREAMTRANSMITTER_H

#include <QObject>
#include <QTcpServer>

class QTimer;

class StreamTransmitter : public QObject
{
    Q_OBJECT
public:
    explicit StreamTransmitter(int interval, int partsCount, QObject *parent = 0);
    void run();
signals:

public slots:

private:
    QByteArray fileToByteArray(QString fileName);
    void sendFewBytes();
    QList<QByteArray> splitToParts(const QByteArray& arr, int parts);

private slots:
    void slNewConnection();
    void readyRead();
    void disconnected();

private:
    QTimer* mIncomingDataTimer;
    QTcpServer* mServer;
    QList<QTcpSocket*> mClients;
    int mTimerInterval;
    int mPartsCount;

};

#endif // STREAMTRANSMITTER_H
